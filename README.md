# `go-style-lint`

[![coverage
report](https://gitlab.com/matthewhughes/go-style-lint/badges/main/coverage.svg)](https://gitlab.com/matthewhughes/go-cov)

A collection of linters (and corresponding
[`pre-commit`](https://pre-commit.com) hooks) for enforcing some certain styles
in Go code

## `bare-nolint`

`bare-nolint` checks for bare
[`//nolint`](https://golangci-lint.run/usage/false-positives/#nolint-directive)
directives in your code. Usage:

``` console
$ bare-nolint <filename>
```

These directives should always include the names of the linters being ignored,
e.g. `//nolint:golint,unused` in order to:

  - More clearly communicate intent behind the exclusion
  - Help identify unnecessary directives, e.g. if a directive is for a linter
    that is no longer run

Compare:

``` go
func helloHandler(w http.ResponseWriter, _ *http.Request) {
	io.WriteString(w, "Hello from a HandleFunc #2!\n") //nolint
}
```

With:

``` go
func helloHandler(w http.ResponseWriter, _ *http.Request) {
	io.WriteString(w, "Hello from a HandleFunc #2!\n") //nolint:errcheck
}

```

The latter helps, at a glance, to suggest the exception has *something* to do
with error checking, and gives anyone curious some more information they can
easily search to find out more.

Installation:

``` console
$ go install gitlab.com/matthewhughes/go-style-lint/cmd/bare-nolint@v0.4.0
```

Pre-commit hook:

``` yaml
-   repo: https://gitlab.com/matthewhughes/go-style-lint
    rev: v0.4.0
    hooks:
    -   id: bare-nolint
```

## `ctx-style`

`ctx-style` checks that function declarations using `context.Context` follow the
[documented style guidelines](https://pkg.go.dev/context#pkg-overview):

> Do not store Contexts inside a struct type; instead, pass a Context explicitly
> to each function that needs it. The Context should be the first parameter,
> typically named ctx

The supported names for the variable are: `ctx`, `_`, and `parent`

Usage:

``` console
$ ctx-style <filename>
```

``` go
// ok: context is first argument named ctx
func withCtx(ctx context.Context) {
    doStuffWithCtx(ctx)
}

// ok: unused context's can use the name '_'
func withCtx(_ context.Context) {
    doStuff()
}

// ok: `parent` is also an acceptable name
func makeNewCtx(parent context.Context) context.Context {
    return enrichContext(parent)
}

// bad: context arg should be named 'ctx'
func withBadFx(myCtx context.Context) {
    doStuffWithCtx(myCtx)
}

// bad: context arg should be first arg
func withBadFx(id int, Ctx context.Context) {
    doStuffWithCtx(id, myCtx)
}
```

You can skip checking for this linter on a specific line with a comment
containing the string `//no-ctx-style:`

``` go
func newCtx(other context.Context) context.Context { //no-ctx-style:
    return enrichCtx(parent)
}

```

Installation:

``` console
$ go install gitlab.com/matthewhughes/go-style-lint/cmd/ctx-style@v0.4.0
```

Pre-commit hook:

``` yaml
-   repo: https://gitlab.com/matthewhughes/go-style-lint
    rev: v0.4.0
    hooks:
    -   id: ctx-style
```
