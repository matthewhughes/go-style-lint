package internal

import (
	"io"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func RunWithCapture(t *testing.T, runnee func([]string) int, args []string) (int, []byte, []byte) {
	t.Helper()
	stdoutr, stdoutw, err := os.Pipe()
	require.NoError(t, err)

	stderrr, stderrw, err := os.Pipe()
	require.NoError(t, err)

	oldOut := os.Stdout
	oldErr := os.Stderr
	os.Stdout = stdoutw
	os.Stderr = stderrw
	t.Cleanup(func() {
		os.Stdout = oldOut
		os.Stderr = oldErr
	})

	returnCode := runnee(args)
	stderrw.Close()
	stdoutw.Close()

	capturedOut, err := io.ReadAll(stdoutr)
	require.NoError(t, err)
	capturedErr, err := io.ReadAll(stderrr)
	require.NoError(t, err)

	return returnCode, capturedOut, capturedErr
}
