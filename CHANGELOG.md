# Changelog

## 0.4.0 - 2023-10-04

### Fixed

  - `ctx-style`: Fix false negative when `context.Context` included with other
    aliased imports

## 0.3.0 - 2023-09-28

### Changed

  - `ctx-style`: accept `parent` as a valid context arg name

## 0.2.0 - 2023-09-24

### Changed

  - `bare-nolint`: also consider `//nolint:` as invalid

## 0.1.0 - 2023-09-24

*Initial release*
