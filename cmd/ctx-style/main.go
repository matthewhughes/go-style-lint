package main

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"os"
	"strings"
)

const (
	ctxName         = "ctx"
	replacementName = "_"
	skipComment     = "//no-ctx-style:"
)

var validNames []string = []string{"ctx", "_", "parent"}

type reason string

const (
	noReason    reason = ""
	badName     reason = "a context.Context argument should be named either 'ctx' or '_'"
	badLocation reason = "a context.Context argument should be the first argument"
)

type badFunc struct {
	r    reason
	line int
	col  int
}

func main() { //go-cov:skip
	os.Exit(run(os.Args))
}

func run(args []string) int {
	retcode := 0

	for _, fname := range args[1:] {
		fset := token.NewFileSet()
		astFile, err := parser.ParseFile(
			fset,
			fname,
			nil,
			parser.ParseComments|parser.SkipObjectResolution,
		)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to parse file %s: %v", fname, err)
			retcode = 1
			continue
		}

		badCtxFuncs := getBadCtxFuncs(fset, astFile)
		skipLines := getSkipLines(fset, astFile)

		if len(badCtxFuncs) != 0 {
			for _, badFunc := range badCtxFuncs {
				if _, ok := skipLines[badFunc.line]; ok {
					continue
				}
				fmt.Fprintf(
					os.Stderr,
					"%s: function with bad Context usage (%s) at (line, col): (%d, %d)\n",
					fname,
					badFunc.r,
					badFunc.line,
					badFunc.col,
				)
				retcode = 1
			}
		}
	}

	return retcode
}

func getSkipLines(fset *token.FileSet, astFile *ast.File) map[int]struct{} {
	skipLines := make(map[int]struct{})
	for _, group := range astFile.Comments {
		for _, comment := range group.List {
			if strings.Contains(comment.Text, skipComment) {
				skipLines[fset.Position(comment.Slash).Line] = struct{}{}
			}
		}
	}

	return skipLines
}

type badCtxFinder struct {
	fset              *token.FileSet
	contextImportName string
	badFuncs          []badFunc
}

func getBadCtxFuncs(fset *token.FileSet, astFile *ast.File) []badFunc {
	finder := badCtxFinder{fset, getContextImportName(astFile), []badFunc{}}
	ast.Walk(&finder, astFile)

	return finder.badFuncs
}

func getContextImportName(astFile *ast.File) string {
	for _, spec := range astFile.Imports {
		if spec.Path.Value == `"context"` && spec.Name != nil {
			return spec.Name.Name
		}
	}
	// don't bother with the case were we forget to import context
	return "context"
}

func (f *badCtxFinder) Visit(node ast.Node) (w ast.Visitor) {
	var ft *ast.FuncType
	switch node := node.(type) {
	case *ast.FuncDecl:
		ft = node.Type
	case *ast.FuncLit:
		ft = node.Type
	default:
		return f
	}

	if bad, r := hasBadCtxArg(ft, f.contextImportName); bad {
		pos := f.fset.Position(node.Pos())
		f.badFuncs = append(f.badFuncs, badFunc{r, pos.Line, pos.Column})
	}

	return f
}

// TODO: do we consider multiple contexts valid? func (ctx, ctx2 context.Context)
func hasBadCtxArg(f *ast.FuncType, contextImportName string) (bool, reason) {
	for i, field := range f.Params.List {
		t, ok := field.Type.(*ast.SelectorExpr)
		if !ok {
			continue
		}
		ident, ok := t.X.(*ast.Ident)
		if !ok { //go-cov:skip TODO: find syntax to cover this
			continue
		}

		if ident.Name != contextImportName || t.Sel.Name != "Context" {
			continue
		}

		hasValidName := false
		for _, name := range validNames {
			if field.Names[0].Name == name {
				hasValidName = true
				break
			}
		}

		if !hasValidName {
			return true, badName
		}

		if i != 0 {
			return true, badLocation
		}
	}

	return false, noReason
}
