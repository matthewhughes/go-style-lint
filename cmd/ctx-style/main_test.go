package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"

	"gitlab.com/matthewhughes/go-style-lint/internal"
)

func TestDetectsBadCtx(t *testing.T) {
	tempDir := t.TempDir()

	for i, tc := range []struct {
		desc        string
		fileContent string
		expected    []badFunc
	}{
		{
			"basic declaration: context in wrong position",
			dedent.Dedent(`package foo

			import (
				"context"
			)

			func withCtx(first int, ctx context.Context) {}
			`),
			[]badFunc{{badLocation, 7, 4}},
		},
		{
			"basic declaration: context wrong name",
			dedent.Dedent(`package foo

			import (
				"context"
			)

			func withCtx(myCtx context.Context) {}
			`),
			[]badFunc{{badName, 7, 4}},
		},
		{
			"basic declaration: context wrong name from alias",
			dedent.Dedent(`package foo

			import (
				c "context"
			)

			func withCtx(myCtx c.Context) {}
			`),
			[]badFunc{{badName, 7, 4}},
		},
		{
			"basic declaration: context wrong name",
			dedent.Dedent(`package foo

			import (
				"context"
			)

			func withCtx(ctx1, ctx2 context.Context) {}
			`),
			[]badFunc{{badName, 7, 4}},
		},
		{
			"method declaration: context in wrong position",
			dedent.Dedent(`package foo

			import (
				"context"
			)

			type f struct {}

			func (_ *f) withCtx(first int, ctx context.Context) {}
			`),
			[]badFunc{{badLocation, 9, 4}},
		},
		{
			"method declaration: context wrong name",
			dedent.Dedent(`package foo

			import (
				"context"
			)

			type f struct {}

			func (_ *f) withCtx(myCtx context.Context) {}
			`),
			[]badFunc{{badName, 9, 4}},
		},
		{
			"inline declaration: context in wrong position",
			dedent.Dedent(`package foo

			import (
				"context"
			)

			func f () {
				h := func (first int, ctx context.Context) {}
			}
			`),
			[]badFunc{{badLocation, 8, 10}},
		},
		{
			"inline declaration: context wrong name",
			dedent.Dedent(`package foo

			import (
				"context"
			)

			func f () {
				h := func (myCtx context.Context) {}
			}
			`),
			[]badFunc{{badName, 8, 10}},
		},
		{
			"multiple basic declarations: context in wrong position",
			dedent.Dedent(`package foo

			import (
				"context"
			)

			func withCtx(first int, ctx context.Context) {}
			func withCtx2(myCtx context.Context) {}
			`),
			[]badFunc{{badLocation, 7, 4}, {badName, 8, 4}},
		},
		{
			"basic declaration: context in wrong position, multiple imports",
			dedent.Dedent(`package foo

			import (
				"log"
				"context"
			)

			func withCtx(first int, ctx context.Context) {}
			`),
			[]badFunc{{badLocation, 8, 4}},
		},
		{
			"basic declaration: context in wrong position, multiple imports with names",
			dedent.Dedent(`package foo

			import (
				l "log"
				"context"
			)

			func withCtx(first int, ctx context.Context) {}
			`),
			[]badFunc{{badLocation, 8, 4}},
		},
	} {
		t.Run(tc.desc, func(t *testing.T) {
			fname := filepath.Join(tempDir, fmt.Sprintf("file%d.go", i))
			require.NoError(t, os.WriteFile(fname, []byte(tc.fileContent), 0o600))

			var expectedErrs []string
			for _, badFunc := range tc.expected {
				expectedErrs = append(
					expectedErrs,
					fname+": function with bad Context usage ("+string(
						badFunc.r,
					)+") at (line, col): ("+fmt.Sprintf(
						"%d, %d",
						badFunc.line,
						badFunc.col,
					)+")",
				)
			}

			returnCode, stdOut, stdErr := internal.RunWithCapture(
				t,
				run,
				[]string{"prog-name", fname},
			)

			require.Equal(t, 1, returnCode)
			require.Equal(t, "", string(stdOut))
			gotErrLines := strings.Split(string(stdErr), "\n")
			require.Equal(t, expectedErrs, gotErrLines[:len(gotErrLines)-1])
		})
	}
}

func TestAcceptsValidCtx(t *testing.T) {
	tempDir := t.TempDir()

	for i, tc := range []struct {
		desc        string
		fileContent string
	}{
		{
			"basic function 'ctx' name",
			dedent.Dedent(`package foo

			import (
				"context"
			)

			func withCtx(ctx context.Context) {}
			`),
		},
		{
			"basic function, unnamed",
			dedent.Dedent(`package foo

			import (
				"context"
			)

			func withCtx(_ context.Context) {}
			`),
		},
		{
			"basic function 'parent' name",
			dedent.Dedent(`package foo

			import (
				"context"
			)

			func withCtx(parent context.Context) {}
			`),
		},
		{
			"basic method, 'ctx' name",
			dedent.Dedent(`package foo

			import (
				"context"
			)

			type f struct {}

			func (_ *f) withCtx(ctx context.Context) {}
			`),
		},
		{
			"basic method, unnamed",
			dedent.Dedent(`package foo

			import (
				"context"
			)

			type f struct {}

			func (_ *f) withCtx(_ context.Context) {}
			`),
		},
		{
			"basic method, 'parent' name",
			dedent.Dedent(`package foo

			import (
				"context"
			)

			type f struct {}

			func (_ *f) withCtx(parent context.Context) {}
			`),
		},
		{
			"function with multiple args",
			dedent.Dedent(`package foo

			import (
				"context"
			)

			func withCtx(ctx context.Context, otherVar int) {}
			`),
		},
		{
			"function with multiple 'ctx' args",
			dedent.Dedent(`package foo

			import (
				"context"
			)

			func withCtx(ctx context.Context, ctx int) {}
			`),
		},
		{
			"multiple functions, 'ctx' name",
			dedent.Dedent(`package foo

			import (
				"context"
			)

			func withCtx(ctx context.Context) {}
			func withCtx2(ctx context.Context) {}
			`),
		},
		{
			"bad name, skip comment",
			dedent.Dedent(`package foo

			import (
				"context"
			)

			func withCtx(myCtx context.Context, ctx int) {} //no-ctx-style:
			`),
		},
		{
			"function with not context",
			dedent.Dedent(`package foo

			import (
				"context"
			)

			func someOtherFunc(x int) {}
			`),
		},
		{
			"function with other 'context' type",
			dedent.Dedent(`package foo

			import (
				"context"
			)

			func canceller(f context.CancelFunc) {}
			`),
		},
		{
			"basic function 'ctx' name, missing 'context' import",
			dedent.Dedent(`package foo

			func withCtx(ctx context.Context) {}
			`),
		},
		{
			"basic function 'ctx' name, multiple imports",
			dedent.Dedent(`package foo

			import (
				"log"
				"context"
			)

			func withCtx(ctx context.Context) {}
			`),
		},
		{
			"basic function 'ctx' name, multiple aliases imports",
			dedent.Dedent(`package foo

			import (
				l "log"
				"context"
			)

			func withCtx(ctx context.Context) {}
			`),
		},
		{
			"basic function 'ctx' name, packae from alias",
			dedent.Dedent(`package foo

			import (
				c "context"
			)

			func withCtx(ctx c.Context) {}
			`),
		},
		{
			"basic function, no context but same name from alias",
			dedent.Dedent(`package foo

			import (
				context "example.com/some/pkg"
			)

			func withCtx(myCtx context.ctx) {}
			`),
		},
	} {
		t.Run(tc.desc, func(t *testing.T) {
			fname := filepath.Join(tempDir, fmt.Sprintf("file%d.go", i))
			require.NoError(t, os.WriteFile(fname, []byte(tc.fileContent), 0o600))

			returnCode, stdOut, stdErr := internal.RunWithCapture(
				t,
				run,
				[]string{"prog-name", fname},
			)

			require.Equal(t, 0, returnCode)
			require.Equal(t, "", string(stdOut))
			require.Equal(t, "", string(stdErr))
		})
	}
}

func TestFailsOnUnredableFile(t *testing.T) {
	fname := filepath.Join(t.TempDir(), "file.go")
	fileContent := "NOT VALID GO\n"
	expectedErrPrefix := "Failed to parse file " + fname + ":"
	require.NoError(t, os.WriteFile(fname, []byte(fileContent), 0o600))

	retCode, gotOut, gotErr := internal.RunWithCapture(t, run, []string{"prog-name", fname})

	require.Equal(t, 1, retCode)
	require.Equal(t, "", string(gotOut))
	require.Contains(t, string(gotErr), expectedErrPrefix)
}
