package main

import (
	"fmt"
	"go/parser"
	"go/token"
	"os"
	"regexp"
)

var nolintCommentRe = regexp.MustCompile("// ?nolint:?(?: |$)")

func main() { //go-cov:skip
	os.Exit(run(os.Args))
}

func run(args []string) int {
	retcode := 0

	for _, fname := range args[1:] {
		bareNoLintLines, err := getBareNoLintLines(fname)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			retcode = 1
			continue
		}
		if len(bareNoLintLines) != 0 {
			for _, lineNum := range bareNoLintLines {
				fmt.Fprintf(os.Stderr, "%s: Bare //nolint at line %d\n", fname, lineNum)
			}
			retcode = 1
		}
	}
	return retcode
}

func getBareNoLintLines(fname string) ([]int, error) {
	fset := token.NewFileSet()
	astFile, err := parser.ParseFile(
		fset,
		fname,
		nil,
		parser.ParseComments|parser.SkipObjectResolution,
	)
	if err != nil {
		return nil, fmt.Errorf("Failed to parse file %s: %v", fname, err)
	}

	var bareNoLintLines []int
	for _, group := range astFile.Comments {
		for _, comment := range group.List {
			if nolintCommentRe.MatchString(comment.Text) {
				bareNoLintLines = append(bareNoLintLines, fset.Position(comment.Slash).Line)
			}
		}
	}

	return bareNoLintLines, nil
}
